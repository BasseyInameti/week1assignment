Chanpackage week1_assignment.src.assignment2;

// Write a java program that calculates the mean, median and mode of a given array.

public class assignment {
    /**
     * @param args
     */
    public static void main(String[] args){
        int array[] = {13,6,9,2,4,3,1,5,15,12,12,1,4,6,5};
        sandbox stat = new sandbox();

         double mean= stat.calculateMean(array);
         double median = stat.findMedian(array);
         double mode = stat.calculateMode(array);

         System.out.println("this is the mean "+ mean);
         System.out.println("this is the median "+ median);
         System.out.println("this is the mode "+ mode);
        stat.findRepetition(array);

    
    }

}
