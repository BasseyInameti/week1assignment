package week1_assignment.src.assignment2;
import java.util.Arrays;

public class sandbox {

    public double calculateMean(int[] a){
        double sum =0;
        Arrays.sort(a);
        for (int counter=0; counter<a.length; counter++){
            sum+=a[counter]; 
            System.out.println(a[counter]);
        }
        double mean= sum/a.length;
        
        return  Math.round(mean * 100.0) / 100.0;
    }
    public  double findMedian(int[] a){
        double median=0;
        Arrays.sort(a);

        double middleNumber = ((double) a.length) /2;
        if ((a.length % 2) != 0 ){
            median=a[(int) middleNumber];
            
        }else{
            median= ((a[ ((int)middleNumber)-1] + a[ (int)middleNumber])/2);
            
        }

        return median;
    }

    public Double calculateMode(int a[]) {
        double maxValue = 0, maxCount = 0, n = a.length;
        for (int i = 0; i < (n); ++i) 

        {
            int count = 0;
            for (int j = 0; j < n; ++j){

                if (a[j] == a[i]){
                    ++count;   
                }
            }

            if (count > maxCount){
                // System.out.println("this is"+maxValue);
                maxCount = count;
                maxValue = a[i];
            }
        }
        return maxValue;
    }

    public void findRepetition(int[] a) {
        Arrays.sort(a);
        int arrayLength = a.length;
        int freq[] = new int[arrayLength];//length of new array

        for (int i=0; i<arrayLength; i++){
            int count =0;
            for (int j =0; j< arrayLength; ++j){
                if (a[j]==a[i]){
                    ++count;
                    ++freq[i];
                }
                
            }
        }
        for (int j=0; j<freq.length - 1; j++){
            if (a[j] != a[j+1]){  
                System.out.println(a[j]+"\t"+freq[j]);
            }        
        }
    }
}
